/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Warehouse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.Warehouse#getOnlineshop <em>Onlineshop</em>}</li>
 *   <li>{@link com.comtek.pavel.onlineshop.Warehouse#getProductCount <em>Product Count</em>}</li>
 * </ul>
 *
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getWarehouse()
 * @model
 * @generated
 */
public interface Warehouse extends Building {
	/**
	 * Returns the value of the '<em><b>Onlineshop</b></em>' reference list.
	 * The list contents are of type {@link com.comtek.pavel.onlineshop.OnlineShop}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Onlineshop</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Onlineshop</em>' reference list.
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getWarehouse_Onlineshop()
	 * @model
	 * @generated
	 */
	EList<OnlineShop> getOnlineshop();

	/**
	 * Returns the value of the '<em><b>Product Count</b></em>' containment reference list.
	 * The list contents are of type {@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Product Count</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Product Count</em>' containment reference list.
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getWarehouse_ProductCount()
	 * @model containment="true"
	 * @generated
	 */
	EList<EStringToEIntegerObjectMap> getProductCount();

} // Warehouse
