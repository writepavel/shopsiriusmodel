/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.Customer;
import com.comtek.pavel.onlineshop.OnlineShop;
import com.comtek.pavel.onlineshop.OnlineshopPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Online Shop</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.impl.OnlineShopImpl#getCustomer <em>Customer</em>}</li>
 *   <li>{@link com.comtek.pavel.onlineshop.impl.OnlineShopImpl#getProductCode <em>Product Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OnlineShopImpl extends MinimalEObjectImpl.Container implements OnlineShop {
	/**
	 * The cached value of the '{@link #getCustomer() <em>Customer</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomer()
	 * @generated
	 * @ordered
	 */
	protected EList<Customer> customer;

	/**
	 * The cached value of the '{@link #getProductCode() <em>Product Code</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductCode()
	 * @generated
	 * @ordered
	 */
	protected EList<String> productCode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OnlineShopImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OnlineshopPackage.Literals.ONLINE_SHOP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Customer> getCustomer() {
		if (customer == null) {
			customer = new EObjectResolvingEList<Customer>(Customer.class, this, OnlineshopPackage.ONLINE_SHOP__CUSTOMER);
		}
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getProductCode() {
		if (productCode == null) {
			productCode = new EDataTypeUniqueEList<String>(String.class, this, OnlineshopPackage.ONLINE_SHOP__PRODUCT_CODE);
		}
		return productCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OnlineshopPackage.ONLINE_SHOP__CUSTOMER:
				return getCustomer();
			case OnlineshopPackage.ONLINE_SHOP__PRODUCT_CODE:
				return getProductCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OnlineshopPackage.ONLINE_SHOP__CUSTOMER:
				getCustomer().clear();
				getCustomer().addAll((Collection<? extends Customer>)newValue);
				return;
			case OnlineshopPackage.ONLINE_SHOP__PRODUCT_CODE:
				getProductCode().clear();
				getProductCode().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.ONLINE_SHOP__CUSTOMER:
				getCustomer().clear();
				return;
			case OnlineshopPackage.ONLINE_SHOP__PRODUCT_CODE:
				getProductCode().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.ONLINE_SHOP__CUSTOMER:
				return customer != null && !customer.isEmpty();
			case OnlineshopPackage.ONLINE_SHOP__PRODUCT_CODE:
				return productCode != null && !productCode.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (productCode: ");
		result.append(productCode);
		result.append(')');
		return result.toString();
	}

} //OnlineShopImpl
