/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.Building;
import com.comtek.pavel.onlineshop.Customer;
import com.comtek.pavel.onlineshop.CustomerHome;
import com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap;
import com.comtek.pavel.onlineshop.OnlineShop;
import com.comtek.pavel.onlineshop.OnlineshopFactory;
import com.comtek.pavel.onlineshop.OnlineshopPackage;
import com.comtek.pavel.onlineshop.Warehouse;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OnlineshopPackageImpl extends EPackageImpl implements OnlineshopPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customerHomeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass warehouseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass onlineShopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eStringToEIntegerObjectMapEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OnlineshopPackageImpl() {
		super(eNS_URI, OnlineshopFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OnlineshopPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OnlineshopPackage init() {
		if (isInited) return (OnlineshopPackage)EPackage.Registry.INSTANCE.getEPackage(OnlineshopPackage.eNS_URI);

		// Obtain or create and register package
		OnlineshopPackageImpl theOnlineshopPackage = (OnlineshopPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OnlineshopPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OnlineshopPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theOnlineshopPackage.createPackageContents();

		// Initialize created meta-data
		theOnlineshopPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOnlineshopPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OnlineshopPackage.eNS_URI, theOnlineshopPackage);
		return theOnlineshopPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomer() {
		return customerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCustomer_Customerhome() {
		return (EReference)customerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCustomer__BuyProduct__String_Integer() {
		return customerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomerHome() {
		return customerHomeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuilding() {
		return buildingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuilding_Address() {
		return (EAttribute)buildingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWarehouse() {
		return warehouseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWarehouse_Onlineshop() {
		return (EReference)warehouseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWarehouse_ProductCount() {
		return (EReference)warehouseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOnlineShop() {
		return onlineShopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOnlineShop_Customer() {
		return (EReference)onlineShopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOnlineShop_ProductCode() {
		return (EAttribute)onlineShopEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEStringToEIntegerObjectMap() {
		return eStringToEIntegerObjectMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEStringToEIntegerObjectMap_Key() {
		return (EAttribute)eStringToEIntegerObjectMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEStringToEIntegerObjectMap_Value() {
		return (EAttribute)eStringToEIntegerObjectMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OnlineshopFactory getOnlineshopFactory() {
		return (OnlineshopFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		customerEClass = createEClass(CUSTOMER);
		createEReference(customerEClass, CUSTOMER__CUSTOMERHOME);
		createEOperation(customerEClass, CUSTOMER___BUY_PRODUCT__STRING_INTEGER);

		customerHomeEClass = createEClass(CUSTOMER_HOME);

		buildingEClass = createEClass(BUILDING);
		createEAttribute(buildingEClass, BUILDING__ADDRESS);

		warehouseEClass = createEClass(WAREHOUSE);
		createEReference(warehouseEClass, WAREHOUSE__ONLINESHOP);
		createEReference(warehouseEClass, WAREHOUSE__PRODUCT_COUNT);

		onlineShopEClass = createEClass(ONLINE_SHOP);
		createEReference(onlineShopEClass, ONLINE_SHOP__CUSTOMER);
		createEAttribute(onlineShopEClass, ONLINE_SHOP__PRODUCT_CODE);

		eStringToEIntegerObjectMapEClass = createEClass(ESTRING_TO_EINTEGER_OBJECT_MAP);
		createEAttribute(eStringToEIntegerObjectMapEClass, ESTRING_TO_EINTEGER_OBJECT_MAP__KEY);
		createEAttribute(eStringToEIntegerObjectMapEClass, ESTRING_TO_EINTEGER_OBJECT_MAP__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		customerHomeEClass.getESuperTypes().add(this.getBuilding());
		warehouseEClass.getESuperTypes().add(this.getBuilding());

		// Initialize classes, features, and operations; add parameters
		initEClass(customerEClass, Customer.class, "Customer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCustomer_Customerhome(), this.getCustomerHome(), null, "customerhome", null, 0, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getCustomer__BuyProduct__String_Integer(), null, "buyProduct", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "productCode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "count", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(customerHomeEClass, CustomerHome.class, "CustomerHome", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(buildingEClass, Building.class, "Building", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBuilding_Address(), ecorePackage.getEString(), "Address", null, 0, 1, Building.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(warehouseEClass, Warehouse.class, "Warehouse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWarehouse_Onlineshop(), this.getOnlineShop(), null, "onlineshop", null, 0, -1, Warehouse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWarehouse_ProductCount(), this.getEStringToEIntegerObjectMap(), null, "productCount", null, 0, -1, Warehouse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(onlineShopEClass, OnlineShop.class, "OnlineShop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOnlineShop_Customer(), this.getCustomer(), null, "customer", null, 0, -1, OnlineShop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOnlineShop_ProductCode(), ecorePackage.getEString(), "productCode", null, 0, -1, OnlineShop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eStringToEIntegerObjectMapEClass, EStringToEIntegerObjectMap.class, "EStringToEIntegerObjectMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEStringToEIntegerObjectMap_Key(), ecorePackage.getEString(), "key", null, 0, 1, EStringToEIntegerObjectMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEStringToEIntegerObjectMap_Value(), ecorePackage.getEIntegerObject(), "value", null, 0, 1, EStringToEIntegerObjectMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OnlineshopPackageImpl
