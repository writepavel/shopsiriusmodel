/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OnlineshopFactoryImpl extends EFactoryImpl implements OnlineshopFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OnlineshopFactory init() {
		try {
			OnlineshopFactory theOnlineshopFactory = (OnlineshopFactory)EPackage.Registry.INSTANCE.getEFactory(OnlineshopPackage.eNS_URI);
			if (theOnlineshopFactory != null) {
				return theOnlineshopFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OnlineshopFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OnlineshopFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OnlineshopPackage.CUSTOMER: return createCustomer();
			case OnlineshopPackage.CUSTOMER_HOME: return createCustomerHome();
			case OnlineshopPackage.WAREHOUSE: return createWarehouse();
			case OnlineshopPackage.ONLINE_SHOP: return createOnlineShop();
			case OnlineshopPackage.ESTRING_TO_EINTEGER_OBJECT_MAP: return createEStringToEIntegerObjectMap();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer createCustomer() {
		CustomerImpl customer = new CustomerImpl();
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomerHome createCustomerHome() {
		CustomerHomeImpl customerHome = new CustomerHomeImpl();
		return customerHome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Warehouse createWarehouse() {
		WarehouseImpl warehouse = new WarehouseImpl();
		return warehouse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OnlineShop createOnlineShop() {
		OnlineShopImpl onlineShop = new OnlineShopImpl();
		return onlineShop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStringToEIntegerObjectMap createEStringToEIntegerObjectMap() {
		EStringToEIntegerObjectMapImpl eStringToEIntegerObjectMap = new EStringToEIntegerObjectMapImpl();
		return eStringToEIntegerObjectMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OnlineshopPackage getOnlineshopPackage() {
		return (OnlineshopPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OnlineshopPackage getPackage() {
		return OnlineshopPackage.eINSTANCE;
	}

} //OnlineshopFactoryImpl
