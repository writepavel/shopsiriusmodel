/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.Customer;
import com.comtek.pavel.onlineshop.CustomerHome;
import com.comtek.pavel.onlineshop.OnlineshopPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.impl.CustomerImpl#getCustomerhome <em>Customerhome</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CustomerImpl extends MinimalEObjectImpl.Container implements Customer {
	/**
	 * The cached value of the '{@link #getCustomerhome() <em>Customerhome</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomerhome()
	 * @generated
	 * @ordered
	 */
	protected CustomerHome customerhome;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OnlineshopPackage.Literals.CUSTOMER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomerHome getCustomerhome() {
		if (customerhome != null && customerhome.eIsProxy()) {
			InternalEObject oldCustomerhome = (InternalEObject)customerhome;
			customerhome = (CustomerHome)eResolveProxy(oldCustomerhome);
			if (customerhome != oldCustomerhome) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OnlineshopPackage.CUSTOMER__CUSTOMERHOME, oldCustomerhome, customerhome));
			}
		}
		return customerhome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomerHome basicGetCustomerhome() {
		return customerhome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomerhome(CustomerHome newCustomerhome) {
		CustomerHome oldCustomerhome = customerhome;
		customerhome = newCustomerhome;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OnlineshopPackage.CUSTOMER__CUSTOMERHOME, oldCustomerhome, customerhome));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void buyProduct(String productCode, Integer count) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OnlineshopPackage.CUSTOMER__CUSTOMERHOME:
				if (resolve) return getCustomerhome();
				return basicGetCustomerhome();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OnlineshopPackage.CUSTOMER__CUSTOMERHOME:
				setCustomerhome((CustomerHome)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.CUSTOMER__CUSTOMERHOME:
				setCustomerhome((CustomerHome)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.CUSTOMER__CUSTOMERHOME:
				return customerhome != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OnlineshopPackage.CUSTOMER___BUY_PRODUCT__STRING_INTEGER:
				buyProduct((String)arguments.get(0), (Integer)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //CustomerImpl
