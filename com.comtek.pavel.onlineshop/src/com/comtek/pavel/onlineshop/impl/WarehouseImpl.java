/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap;
import com.comtek.pavel.onlineshop.OnlineShop;
import com.comtek.pavel.onlineshop.OnlineshopPackage;
import com.comtek.pavel.onlineshop.Warehouse;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Warehouse</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.impl.WarehouseImpl#getOnlineshop <em>Onlineshop</em>}</li>
 *   <li>{@link com.comtek.pavel.onlineshop.impl.WarehouseImpl#getProductCount <em>Product Count</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WarehouseImpl extends BuildingImpl implements Warehouse {
	/**
	 * The cached value of the '{@link #getOnlineshop() <em>Onlineshop</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOnlineshop()
	 * @generated
	 * @ordered
	 */
	protected EList<OnlineShop> onlineshop;

	/**
	 * The cached value of the '{@link #getProductCount() <em>Product Count</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProductCount()
	 * @generated
	 * @ordered
	 */
	protected EList<EStringToEIntegerObjectMap> productCount;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WarehouseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OnlineshopPackage.Literals.WAREHOUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OnlineShop> getOnlineshop() {
		if (onlineshop == null) {
			onlineshop = new EObjectResolvingEList<OnlineShop>(OnlineShop.class, this, OnlineshopPackage.WAREHOUSE__ONLINESHOP);
		}
		return onlineshop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EStringToEIntegerObjectMap> getProductCount() {
		if (productCount == null) {
			productCount = new EObjectContainmentEList<EStringToEIntegerObjectMap>(EStringToEIntegerObjectMap.class, this, OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT);
		}
		return productCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT:
				return ((InternalEList<?>)getProductCount()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OnlineshopPackage.WAREHOUSE__ONLINESHOP:
				return getOnlineshop();
			case OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT:
				return getProductCount();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OnlineshopPackage.WAREHOUSE__ONLINESHOP:
				getOnlineshop().clear();
				getOnlineshop().addAll((Collection<? extends OnlineShop>)newValue);
				return;
			case OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT:
				getProductCount().clear();
				getProductCount().addAll((Collection<? extends EStringToEIntegerObjectMap>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.WAREHOUSE__ONLINESHOP:
				getOnlineshop().clear();
				return;
			case OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT:
				getProductCount().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OnlineshopPackage.WAREHOUSE__ONLINESHOP:
				return onlineshop != null && !onlineshop.isEmpty();
			case OnlineshopPackage.WAREHOUSE__PRODUCT_COUNT:
				return productCount != null && !productCount.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //WarehouseImpl
