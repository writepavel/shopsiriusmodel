/**
 */
package com.comtek.pavel.onlineshop.impl;

import com.comtek.pavel.onlineshop.CustomerHome;
import com.comtek.pavel.onlineshop.OnlineshopPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Customer Home</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CustomerHomeImpl extends BuildingImpl implements CustomerHome {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomerHomeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OnlineshopPackage.Literals.CUSTOMER_HOME;
	}

} //CustomerHomeImpl
