/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage
 * @generated
 */
public interface OnlineshopFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OnlineshopFactory eINSTANCE = com.comtek.pavel.onlineshop.impl.OnlineshopFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Customer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Customer</em>'.
	 * @generated
	 */
	Customer createCustomer();

	/**
	 * Returns a new object of class '<em>Customer Home</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Customer Home</em>'.
	 * @generated
	 */
	CustomerHome createCustomerHome();

	/**
	 * Returns a new object of class '<em>Warehouse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Warehouse</em>'.
	 * @generated
	 */
	Warehouse createWarehouse();

	/**
	 * Returns a new object of class '<em>Online Shop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Online Shop</em>'.
	 * @generated
	 */
	OnlineShop createOnlineShop();

	/**
	 * Returns a new object of class '<em>EString To EInteger Object Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EString To EInteger Object Map</em>'.
	 * @generated
	 */
	EStringToEIntegerObjectMap createEStringToEIntegerObjectMap();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OnlineshopPackage getOnlineshopPackage();

} //OnlineshopFactory
