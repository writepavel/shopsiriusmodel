/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Online Shop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.OnlineShop#getCustomer <em>Customer</em>}</li>
 *   <li>{@link com.comtek.pavel.onlineshop.OnlineShop#getProductCode <em>Product Code</em>}</li>
 * </ul>
 *
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getOnlineShop()
 * @model
 * @generated
 */
public interface OnlineShop extends EObject {
	/**
	 * Returns the value of the '<em><b>Customer</b></em>' reference list.
	 * The list contents are of type {@link com.comtek.pavel.onlineshop.Customer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customer</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer</em>' reference list.
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getOnlineShop_Customer()
	 * @model
	 * @generated
	 */
	EList<Customer> getCustomer();

	/**
	 * Returns the value of the '<em><b>Product Code</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Product Code</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Product Code</em>' attribute list.
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getOnlineShop_ProductCode()
	 * @model
	 * @generated
	 */
	EList<String> getProductCode();

} // OnlineShop
