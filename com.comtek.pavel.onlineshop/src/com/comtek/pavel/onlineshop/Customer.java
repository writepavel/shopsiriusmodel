/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.Customer#getCustomerhome <em>Customerhome</em>}</li>
 * </ul>
 *
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getCustomer()
 * @model
 * @generated
 */
public interface Customer extends EObject {
	/**
	 * Returns the value of the '<em><b>Customerhome</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customerhome</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customerhome</em>' reference.
	 * @see #setCustomerhome(CustomerHome)
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getCustomer_Customerhome()
	 * @model
	 * @generated
	 */
	CustomerHome getCustomerhome();

	/**
	 * Sets the value of the '{@link com.comtek.pavel.onlineshop.Customer#getCustomerhome <em>Customerhome</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Customerhome</em>' reference.
	 * @see #getCustomerhome()
	 * @generated
	 */
	void setCustomerhome(CustomerHome value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void buyProduct(String productCode, Integer count);

} // Customer
