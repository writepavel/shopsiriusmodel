/**
 */
package com.comtek.pavel.onlineshop;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Customer Home</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getCustomerHome()
 * @model
 * @generated
 */
public interface CustomerHome extends Building {
} // CustomerHome
