/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.comtek.pavel.onlineshop.OnlineshopFactory
 * @model kind="package"
 * @generated
 */
public interface OnlineshopPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "onlineshop";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/sirius/pavel/basicfamily";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "onlineshop";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OnlineshopPackage eINSTANCE = com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.CustomerImpl <em>Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.CustomerImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getCustomer()
	 * @generated
	 */
	int CUSTOMER = 0;

	/**
	 * The feature id for the '<em><b>Customerhome</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__CUSTOMERHOME = 0;

	/**
	 * The number of structural features of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Buy Product</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___BUY_PRODUCT__STRING_INTEGER = 0;

	/**
	 * The number of operations of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.BuildingImpl <em>Building</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.BuildingImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getBuilding()
	 * @generated
	 */
	int BUILDING = 2;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING__ADDRESS = 0;

	/**
	 * The number of structural features of the '<em>Building</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Building</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.CustomerHomeImpl <em>Customer Home</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.CustomerHomeImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getCustomerHome()
	 * @generated
	 */
	int CUSTOMER_HOME = 1;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_HOME__ADDRESS = BUILDING__ADDRESS;

	/**
	 * The number of structural features of the '<em>Customer Home</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_HOME_FEATURE_COUNT = BUILDING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Customer Home</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_HOME_OPERATION_COUNT = BUILDING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.WarehouseImpl <em>Warehouse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.WarehouseImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getWarehouse()
	 * @generated
	 */
	int WAREHOUSE = 3;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAREHOUSE__ADDRESS = BUILDING__ADDRESS;

	/**
	 * The feature id for the '<em><b>Onlineshop</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAREHOUSE__ONLINESHOP = BUILDING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Product Count</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAREHOUSE__PRODUCT_COUNT = BUILDING_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Warehouse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAREHOUSE_FEATURE_COUNT = BUILDING_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Warehouse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAREHOUSE_OPERATION_COUNT = BUILDING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.OnlineShopImpl <em>Online Shop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.OnlineShopImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getOnlineShop()
	 * @generated
	 */
	int ONLINE_SHOP = 4;

	/**
	 * The feature id for the '<em><b>Customer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONLINE_SHOP__CUSTOMER = 0;

	/**
	 * The feature id for the '<em><b>Product Code</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONLINE_SHOP__PRODUCT_CODE = 1;

	/**
	 * The number of structural features of the '<em>Online Shop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONLINE_SHOP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Online Shop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONLINE_SHOP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.comtek.pavel.onlineshop.impl.EStringToEIntegerObjectMapImpl <em>EString To EInteger Object Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.comtek.pavel.onlineshop.impl.EStringToEIntegerObjectMapImpl
	 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getEStringToEIntegerObjectMap()
	 * @generated
	 */
	int ESTRING_TO_EINTEGER_OBJECT_MAP = 5;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRING_TO_EINTEGER_OBJECT_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRING_TO_EINTEGER_OBJECT_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EString To EInteger Object Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRING_TO_EINTEGER_OBJECT_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>EString To EInteger Object Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ESTRING_TO_EINTEGER_OBJECT_MAP_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer</em>'.
	 * @see com.comtek.pavel.onlineshop.Customer
	 * @generated
	 */
	EClass getCustomer();

	/**
	 * Returns the meta object for the reference '{@link com.comtek.pavel.onlineshop.Customer#getCustomerhome <em>Customerhome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Customerhome</em>'.
	 * @see com.comtek.pavel.onlineshop.Customer#getCustomerhome()
	 * @see #getCustomer()
	 * @generated
	 */
	EReference getCustomer_Customerhome();

	/**
	 * Returns the meta object for the '{@link com.comtek.pavel.onlineshop.Customer#buyProduct(java.lang.String, java.lang.Integer) <em>Buy Product</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Buy Product</em>' operation.
	 * @see com.comtek.pavel.onlineshop.Customer#buyProduct(java.lang.String, java.lang.Integer)
	 * @generated
	 */
	EOperation getCustomer__BuyProduct__String_Integer();

	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.CustomerHome <em>Customer Home</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer Home</em>'.
	 * @see com.comtek.pavel.onlineshop.CustomerHome
	 * @generated
	 */
	EClass getCustomerHome();

	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.Building <em>Building</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Building</em>'.
	 * @see com.comtek.pavel.onlineshop.Building
	 * @generated
	 */
	EClass getBuilding();

	/**
	 * Returns the meta object for the attribute '{@link com.comtek.pavel.onlineshop.Building#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see com.comtek.pavel.onlineshop.Building#getAddress()
	 * @see #getBuilding()
	 * @generated
	 */
	EAttribute getBuilding_Address();

	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.Warehouse <em>Warehouse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Warehouse</em>'.
	 * @see com.comtek.pavel.onlineshop.Warehouse
	 * @generated
	 */
	EClass getWarehouse();

	/**
	 * Returns the meta object for the reference list '{@link com.comtek.pavel.onlineshop.Warehouse#getOnlineshop <em>Onlineshop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Onlineshop</em>'.
	 * @see com.comtek.pavel.onlineshop.Warehouse#getOnlineshop()
	 * @see #getWarehouse()
	 * @generated
	 */
	EReference getWarehouse_Onlineshop();

	/**
	 * Returns the meta object for the containment reference list '{@link com.comtek.pavel.onlineshop.Warehouse#getProductCount <em>Product Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Product Count</em>'.
	 * @see com.comtek.pavel.onlineshop.Warehouse#getProductCount()
	 * @see #getWarehouse()
	 * @generated
	 */
	EReference getWarehouse_ProductCount();

	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.OnlineShop <em>Online Shop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Online Shop</em>'.
	 * @see com.comtek.pavel.onlineshop.OnlineShop
	 * @generated
	 */
	EClass getOnlineShop();

	/**
	 * Returns the meta object for the reference list '{@link com.comtek.pavel.onlineshop.OnlineShop#getCustomer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Customer</em>'.
	 * @see com.comtek.pavel.onlineshop.OnlineShop#getCustomer()
	 * @see #getOnlineShop()
	 * @generated
	 */
	EReference getOnlineShop_Customer();

	/**
	 * Returns the meta object for the attribute list '{@link com.comtek.pavel.onlineshop.OnlineShop#getProductCode <em>Product Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Product Code</em>'.
	 * @see com.comtek.pavel.onlineshop.OnlineShop#getProductCode()
	 * @see #getOnlineShop()
	 * @generated
	 */
	EAttribute getOnlineShop_ProductCode();

	/**
	 * Returns the meta object for class '{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap <em>EString To EInteger Object Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EString To EInteger Object Map</em>'.
	 * @see com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap
	 * @generated
	 */
	EClass getEStringToEIntegerObjectMap();

	/**
	 * Returns the meta object for the attribute '{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getKey()
	 * @see #getEStringToEIntegerObjectMap()
	 * @generated
	 */
	EAttribute getEStringToEIntegerObjectMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getValue()
	 * @see #getEStringToEIntegerObjectMap()
	 * @generated
	 */
	EAttribute getEStringToEIntegerObjectMap_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OnlineshopFactory getOnlineshopFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.CustomerImpl <em>Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.CustomerImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getCustomer()
		 * @generated
		 */
		EClass CUSTOMER = eINSTANCE.getCustomer();

		/**
		 * The meta object literal for the '<em><b>Customerhome</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUSTOMER__CUSTOMERHOME = eINSTANCE.getCustomer_Customerhome();

		/**
		 * The meta object literal for the '<em><b>Buy Product</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUSTOMER___BUY_PRODUCT__STRING_INTEGER = eINSTANCE.getCustomer__BuyProduct__String_Integer();

		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.CustomerHomeImpl <em>Customer Home</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.CustomerHomeImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getCustomerHome()
		 * @generated
		 */
		EClass CUSTOMER_HOME = eINSTANCE.getCustomerHome();

		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.BuildingImpl <em>Building</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.BuildingImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getBuilding()
		 * @generated
		 */
		EClass BUILDING = eINSTANCE.getBuilding();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILDING__ADDRESS = eINSTANCE.getBuilding_Address();

		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.WarehouseImpl <em>Warehouse</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.WarehouseImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getWarehouse()
		 * @generated
		 */
		EClass WAREHOUSE = eINSTANCE.getWarehouse();

		/**
		 * The meta object literal for the '<em><b>Onlineshop</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAREHOUSE__ONLINESHOP = eINSTANCE.getWarehouse_Onlineshop();

		/**
		 * The meta object literal for the '<em><b>Product Count</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAREHOUSE__PRODUCT_COUNT = eINSTANCE.getWarehouse_ProductCount();

		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.OnlineShopImpl <em>Online Shop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.OnlineShopImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getOnlineShop()
		 * @generated
		 */
		EClass ONLINE_SHOP = eINSTANCE.getOnlineShop();

		/**
		 * The meta object literal for the '<em><b>Customer</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ONLINE_SHOP__CUSTOMER = eINSTANCE.getOnlineShop_Customer();

		/**
		 * The meta object literal for the '<em><b>Product Code</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ONLINE_SHOP__PRODUCT_CODE = eINSTANCE.getOnlineShop_ProductCode();

		/**
		 * The meta object literal for the '{@link com.comtek.pavel.onlineshop.impl.EStringToEIntegerObjectMapImpl <em>EString To EInteger Object Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.comtek.pavel.onlineshop.impl.EStringToEIntegerObjectMapImpl
		 * @see com.comtek.pavel.onlineshop.impl.OnlineshopPackageImpl#getEStringToEIntegerObjectMap()
		 * @generated
		 */
		EClass ESTRING_TO_EINTEGER_OBJECT_MAP = eINSTANCE.getEStringToEIntegerObjectMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ESTRING_TO_EINTEGER_OBJECT_MAP__KEY = eINSTANCE.getEStringToEIntegerObjectMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ESTRING_TO_EINTEGER_OBJECT_MAP__VALUE = eINSTANCE.getEStringToEIntegerObjectMap_Value();

	}

} //OnlineshopPackage
