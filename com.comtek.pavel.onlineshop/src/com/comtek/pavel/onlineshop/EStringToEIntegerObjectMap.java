/**
 */
package com.comtek.pavel.onlineshop;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EString To EInteger Object Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getKey <em>Key</em>}</li>
 *   <li>{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getEStringToEIntegerObjectMap()
 * @model
 * @generated
 */
public interface EStringToEIntegerObjectMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(String)
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getEStringToEIntegerObjectMap_Key()
	 * @model
	 * @generated
	 */
	String getKey();

	/**
	 * Sets the value of the '{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Integer)
	 * @see com.comtek.pavel.onlineshop.OnlineshopPackage#getEStringToEIntegerObjectMap_Value()
	 * @model
	 * @generated
	 */
	Integer getValue();

	/**
	 * Sets the value of the '{@link com.comtek.pavel.onlineshop.EStringToEIntegerObjectMap#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Integer value);

} // EStringToEIntegerObjectMap
